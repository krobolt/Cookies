<?php

namespace Ds\Cookies;

use DateTime;

/**
 * Interface AbstractCookieInterface
 *
 * @package Ds\Cookies
 */
interface AbstractCookieInterface
{
    /**
     * Create a new cookie.
     *
     * @param string $name
     * @param $value
     * @param DateTime $expire  Cookie expires
     * @param string|null $path      Cookie path
     * @param string|null $domain    Cookie domain name
     * @param bool|null $secure      Use Secure Flag
     * @param bool|null $httpOnly    Use HttpOnly Flag
     * @return void
     */
    public function setCookie(
        string $name,
        $value,
        DateTime $expire,
        string $path = null,
        string $domain = null,
        bool $secure = null,
        bool $httpOnly = null
    );

    /**
     * Check if cookie is present.
     *
     * @param string $name  Name of cookie
     * @return bool
     */
    public function hasCookie(string $name) : bool;

    /**
     * Returns a cookie value.
     *
     * @param string $name  Name of cookie
     * @param bool $throw   Throw exceptions
     * @throws \Exception
     * @return mixed|bool
     */
    public function getCookie(string $name, $throw = false);

    /**
     * Returns all cookies.
     *
     * @return array
     */
    public function getCookies() : array;

    /**
     * Delete Cookie.
     *
     * @param string $name          Name of cookie
     * @param string|null $path     Path of cookie
     * @param string|null $domain   Cookie Domain
     * @param bool|null $secure     Use Secure Flag
     * @param bool|null $httpOnly   Use HttpOnly Flag
     * @return void
     */
    public function deleteCookie(
        string $name,
        string $path = null,
        string $domain = null,
        bool $secure = null,
        bool $httpOnly = null
    );
}
