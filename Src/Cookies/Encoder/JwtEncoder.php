<?php

namespace Ds\Cookies\Encoder;

use Ds\Cookies\EncoderInterface;
use Ds\Jwt\JwtInterface;

/**
 * Class JwtEncoder
 *
 * Encodes Cookies against Json Web Token (JWT) signature.
 *
 * @package Rs\Cookies\Encoder
 */
class JwtEncoder implements EncoderInterface
{
    /**
     * @var JwtInterface
     */
    public $jwt;

    /**
     * @var array
     */
    public $defaults;

    /**
     * JwtEncoder constructor.
     *
     * @param JwtInterface $jwt
     * @param string $secret
     * @param array $options
     */
    public function __construct(
        JwtInterface $jwt,
        $secret,
        $options = []
    )
    {
        $this->jwt = $jwt;
        $this->defaults = $options;
    }

    /**
     * @inheritdoc
     */
    public function encode($data, array $options = []){

        $opt = $options += $this->defaults;

        if (!is_array($data)){
            $data = ['value' => $data];
        }

        $prep = $this->jwt->createFromArray('HS256',$data);
        return $prep
            ->withNotBefore(strtotime('now'))
            ->withExpires($opt['expire']->getTimestamp())
            ->withAudience($opt['domain'])
            ->getToken();
    }

    /**
     * @inheritdoc
     */
    public function decode($token){

        try{
            $data = $this->jwt->import($token)->verify();
            $data = $this->removeJwtClaims($data);
        }catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }

        if (count($data) === 1 && isset($data['value'])){
            $data = $data['value'];
        }
        return $data;
    }

    /**
     * Removed added JWT claims from payload.
     *
     * @param $data
     * @return mixed
     */
    protected function removeJwtClaims($data){
        unset($data['exp']);
        unset($data['nbf']);
        unset($data['aud']);
        return $data;
    }
}
