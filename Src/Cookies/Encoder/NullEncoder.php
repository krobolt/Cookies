<?php

namespace Ds\Cookies\Encoder;

use Ds\Cookies\EncoderInterface;

/**
 * Cookie NullEncoder
 *
 * Returns the same values that are passed to it.
 *
 * @package Ds\Cookies\Encoder
 */
class NullEncoder implements EncoderInterface
{
    /**
     * @inheritdoc
     */
    public function encode($data, array $options = []){
        return $data;
    }

    /**
     * @inheritdoc
     */
    public function decode($token){
        return $token;
    }
}
