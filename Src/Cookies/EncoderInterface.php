<?php

namespace Ds\Cookies;

/**
 * Interface EncoderInterface
 *
 * Encoders used to encode and decode cookie data.
 *
 * @package Ds\Cookies
 */
interface EncoderInterface
{
    /**
     * Encode data
     *
     * @param $data
     * @param array $options
     * @return string
     */
    public function encode($data, array $options = []);

    /**
     * Decode token.
     *
     * @param $token
     * @return mixed
     */
    public function decode($token);
}
