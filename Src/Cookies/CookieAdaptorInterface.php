<?php

namespace Ds\Cookies;

/**
 * Interface CookieAdaptorInterface
 * @package Ds\Cookies
 */
interface CookieAdaptorInterface extends AbstractCookieInterface
{
    /**
     * With Cookie values
     *
     * @param array $cookies
     */
    public function withCookies(array $cookies);

}
