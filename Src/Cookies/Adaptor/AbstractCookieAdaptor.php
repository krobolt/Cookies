<?php

namespace Ds\Cookies\Adaptor;

use DateTime;
use Ds\Cookies\CookieAdaptorInterface;

/**
 * Abstract Class AbstractCookieAdaptor
 *
 * @package Ds\Cookies\Adaptor
 */
abstract class AbstractCookieAdaptor implements CookieAdaptorInterface
{
    /**
     * @var array
     */
    protected $cookies;

    /**
     * @inheritdoc
     */
    public function withCookies(array $cookies){
        $new = clone $this;
        $updatedCookies = array_merge($cookies,$this->cookies);
        $new->cookies = $updatedCookies;
        return $new;
    }

    /**
     * @inheritdoc
     */
    public function hasCookie(string $name) : bool{

        if (isset($this->cookies[$name])){
            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function getCookie(string $name, $throw = false){
        if ($this->hasCookie($name)){
            return $this->cookies[$name];
        }

        if ($throw){
            throw new \Exception("Unable to locate cookie: {$name}");
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getCookies() : array{
        return $this->cookies;
    }

    /**
     * @inheritdoc
     */
    public function setCookie(
        string $name,
        $value,
        DateTime $expire,
        string $path = null,
        string $domain = null,
        bool $secure = null,
        bool $httpOnly = null
    ){
        \setcookie(
            $name,
            $value,
            $expire->getTimestamp(),
            $path,
            $domain,
            $secure,
            $httpOnly
        );
    }

    /**
     * @inheritdoc
     */
    public function deleteCookie(
        string $name,
        string $path = null,
        string $domain = null,
        bool $secure = null,
        bool $httpOnly = null
    ){

        $this->setCookie(
            $name,'',new DateTime('13 Jan 1970'),$path,$domain,$secure,$httpOnly
        );
    }
}
