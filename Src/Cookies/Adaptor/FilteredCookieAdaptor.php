<?php

namespace Ds\Cookies\Adaptor;

/**
 * Class CookieAdaptor
 *
 * @package Ds\Cookies\Adaptor
 */
class FilteredCookieAdaptor extends AbstractCookieAdaptor
{
    public function __construct()
    {
        $this->cookies = filter_input_array(INPUT_COOKIE);
    }

    public function withCookies(array $cookies)
    {
        $new = clone $this;

        foreach ($cookies as $index => $cookie){
            $new->cookies[$index] = filter_var($cookie, FILTER_SANITIZE_STRING, FILTER_FLAG_EMPTY_STRING_NULL);
        }

        return $new;
    }

}
