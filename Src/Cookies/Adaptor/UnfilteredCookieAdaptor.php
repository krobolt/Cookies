<?php

namespace Ds\Cookies\Adaptor;

/**
 * Class UnfilteredCookieAdaptor
 *
 * @package Ds\Cookies\Adaptor
 */
class UnfilteredCookieAdaptor extends AbstractCookieAdaptor
{
    public function __construct()
    {
        $this->cookies = &$_COOKIE;
    }
}
