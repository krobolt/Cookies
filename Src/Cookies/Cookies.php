<?php

namespace Ds\Cookies;

use DateTime;
use Ds\Cookies\Encoder\NullEncoder;

/**
 * Class Cookies
 * @package Rs\Cookies
 */
class Cookies implements CookieInterface
{
    /**
     * @var CookieAdaptorInterface $cookieAdaptor   Cookie Adaptor.
     */
    public $cookieAdaptor;

    /**
     * @var DateTime $expire    Cookie default expire.
     */
    protected $expire;

    /**
     * @var string $path    Cookie default path.
     */
    protected $path = '';

    /**
     * @var string $domain  Cookie default domain.
     */
    protected $domain = '';

    /**
     * @var bool $secure    Cookie default use secure flag
     */
    protected $secure = false;

    /**
     * @var bool $httpOnly    Cookie default use httpOnly flag
     */
    protected $httpOnly = false;

    /**
     * @var EncoderInterface $encoder Cookie Encoder
     */
    protected $encoder;

    /**
     * Settings/Getters
     */
    use CookiesTrait;

    /**
     * Cookies constructor.
     *
     * @param CookieAdaptorInterface $cookieAdaptor     Cookie Adaptor
     * @param EncoderInterface $encoder                 Cookie Encoder
     */
    public function __construct(CookieAdaptorInterface $cookieAdaptor, EncoderInterface $encoder = null){

        $this->cookieAdaptor = $cookieAdaptor;
        $this->encoder = $encoder;

        if ($this->encoder === null){
            $this->encoder = new NullEncoder();
        }
    }

    /**
     * @inheritdoc
     */
    public function withEncoder(EncoderInterface $encoder) : CookieInterface
    {
        $new = clone $this;
        $new->encoder = $encoder;
        return $new;
    }

    /**
     * @inheritdoc
     */
    public function withDefaults(
        DateTime $expire,
        $path = '',
        $domain = '',
        $secure = false,
        $httpOnly = false): CookieInterface
    {

        $new = clone $this;
        $new->expire = $expire;
        $new->path = $path;
        $new->domain = $domain;
        $new->secure = $secure;
        $new->httpOnly = $httpOnly;
        return $new;
    }

    /**
     * @inheritdoc
     */
    public function getDefaults(
        DateTime $expire = null,
        string $path = null,
        string $domain = null,
        bool $secure = null,
        bool $httpOnly = null
    ) : array
    {
        return [
            'expire' => isset($expire) ? $expire : $this->expire,
            'path' => isset($path) ? $path : $this->path,
            'domain' => isset($domain) ? $domain : $this->domain,
            'secure' => isset($secure) ? $secure : $this->secure,
            'httpOnly' => isset($httpOnly) ? $httpOnly : $this->httpOnly
        ];
    }

    /**
     * @inheritdoc
     */
    public function setCookie(
        string $name,
        $value,
        DateTime $expire = null,
        string $path = null,
        string $domain = null,
        bool $secure = null,
        bool $httpOnly = null
    )
    {

        $params = $this->getDefaults($expire,$path,$domain,$secure,$httpOnly);

        $value = $this->encoder->encode($value, $this->getDefaults());

        $this->cookieAdaptor->setCookie(
            $name,
            $value,
            $params['expire'],
            $params['path'],
            $params['domain'],
            $params['secure'],
            $params['httpOnly']
        );
    }

    /**
     * @inheritdoc
     */
    public function hasCookie(string $name) : bool
    {
        return $this->cookieAdaptor->hasCookie($name);
    }

    /**
     * @inheritdoc
     */
    public function getCookie(string $name, $throw = false)
    {

        $cookie = $this->cookieAdaptor->getCookie($name);

        try{
            $decoded = $this->encoder->decode($cookie);
        }catch (\Exception $e){
            if ($throw){
                throw new \Exception('unable to read cookie');
            }
            return null;
        }

        return $decoded;

    }

    /**
     * @inheritdoc
     */
    public function getCookies() : array
    {
        return $this->cookieAdaptor->getCookies();
    }

    /**
     * @inheritdoc
     */
    public function deleteCookie(
        string $name,
        string $path = null,
        string $domain = null,
        bool $secure = null,
        bool $httpOnly = null
    )
    {
        $params = $this->getDefaults($this->expire, $path, $domain, $secure, $httpOnly);

        $this->cookieAdaptor->deleteCookie(
            $name,
            $params['path'],
            $params['domain'],
            $params['secure'],
            $params['httpOnly']
        );
    }

    /**
     * @inheritdoc
     */
    public function refreshCookie(string $name, DateTime $expire, string $path = null, string $domain = null, bool $secure = null, bool $httpOnly = null){

        if (!$this->hasCookie($name)){
            return false;
        }

        $cookie = $this->getCookie($name);
        $params = $this->getDefaults(null,$path,$domain,$secure,$httpOnly);

        $this->setCookie(
            $name,
            $cookie,
            $expire,
            $params['path'],
            $params['domain'],
            $params['secure'],
            $params['httpOnly']
        );

        return true;
    }
}
