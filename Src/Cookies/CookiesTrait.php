<?php

namespace Ds\Cookies;

use DateTime;

/**
 * Class CookiesTrait
 * @package Ds\Cookies
 */
trait CookiesTrait
{

    /**
     * @inheritdoc
     */
    public function withDefaults(
        DateTime $expire,
        $path = '',
        $domain = '',
        $secure = false,
        $httpOnly = false)
    {

        $new = clone $this;
        $new->expire = $expire;
        $new->path = $path;
        $new->domain = $domain;
        $new->secure = $secure;
        $new->httpOnly = $httpOnly;
        return $new;
    }

    /**
     * With Expires.
     *
     * @param \DateTime $expire
     * @return CookieInterface
     */
    public function withExpire(\DateTime $expire) : CookieInterface
    {
        $new = clone $this;
        $new->expire = $expire;
        return $new;
    }

    /**
     * With path.
     *
     * @param string $path
     * @return CookieInterface
     */
    public function withPath(string $path) : CookieInterface
    {
        $new = clone $this;
        $new->path = $path;
        return $new;
    }

    /**
     * With domain name.
     *
     * @param string $domain
     * @return CookieInterface
     */
    public function withDomain(string $domain) : CookieInterface
    {
        $new = clone $this;
        $new->domain = $domain;
        return $new;
    }

    /**
     * With secure switch.
     *
     * @param bool $secure
     * @return CookieInterface
     */
    public function withSecure(bool $secure) : CookieInterface
    {
        $new = clone $this;
        $new->secure = $secure;
        return $new;
    }

    /**
     * With http only.
     *
     * @param bool $httpOnly
     * @return CookieInterface
     */
    public function withHttpOnly(bool $httpOnly) : CookieInterface
    {
        $new = clone $this;
        $new->httpOnly = $httpOnly;
        return $new;
    }

    /**
     * Return DateTime object.
     *
     * @return DateTime
     */
    public function getExpire() : DateTime
    {
        return $this->expire;
    }

    /**
     * Return active path.
     * @return string
     */
    public function getPath() : string
    {
        return $this->path;
    }

    /**
     * Get Domain name.
     * @return string
     */
    public function getDomain() : string
    {
        return $this->domain;
    }

    /**
     * Get Secure switch.
     * @return bool
     */
    public function getSecure() : bool
    {
        return $this->secure;
    }

    /**
     * Return Http Only.
     * @return bool
     */
    public function getHttpOnly() : bool
    {
        return $this->httpOnly;
    }
}
