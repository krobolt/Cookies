<?php

namespace Ds\Cookies;

use DateTime;

/**
 * Interface CookieInterface
 * @package Ds\Cookies
 */
interface CookieInterface extends AbstractCookieInterface
{
    /**
     * Replace instance of EncoderInterface.
     *
     * @param EncoderInterface $encoder
     * @return CookieInterface
     */
    public function withEncoder(EncoderInterface $encoder) : CookieInterface;

    /**
     * With default cookie options.
     *
     * @param DateTime $expire  Cookie expires
     * @param string $path      Cookie path
     * @param string $domain    Cookie domain name
     * @param bool $secure      Use Secure Flag
     * @param bool $httpOnly    Use HttpOnly Flag
     * @return CookieInterface
     */
    public function withDefaults(
        DateTime $expire,
        $path = '',
        $domain = '',
        $secure = false,
        $httpOnly = false): CookieInterface;

    /**
     * Get default cookie params
     *
     * Override and return default cookie params.
     *
     * @param DateTime|null $expire  Cookie expires
     * @param string|null $path      Cookie path
     * @param string|null $domain    Cookie domain name
     * @param bool|null $secure      Use Secure Flag
     * @param bool|null $httpOnly    Use HttpOnly Flag
     * @return array
     */
    public function getDefaults(
        DateTime $expire = null,
        string $path = null,
        string $domain = null,
        bool $secure = null,
        bool $httpOnly = null
    ) : array;

    /**
     * With Expires.
     *
     * @param \DateTime $expire
     * @return CookieInterface
     */
    public function withExpire(\DateTime $expire) : CookieInterface;

    /**
     * With path.
     *
     * @param string $path
     * @return CookieInterface
     */
    public function withPath(string $path) : CookieInterface;

    /**
     * With domain name.
     *
     * @param string $domain
     * @return CookieInterface
     */
    public function withDomain(string $domain) : CookieInterface;

    /**
     * With secure switch.
     *
     * @param bool $secure
     * @return CookieInterface
     */
    public function withSecure(bool $secure) : CookieInterface;

    /**
     * With http only.
     *
     * @param bool $httpOnly
     * @return CookieInterface
     */
    public function withHttpOnly(bool $httpOnly) : CookieInterface;

    /**
     * Return DateTime object.
     *
     * @return DateTime
     */
    public function getExpire() : DateTime;

    /**
     * Return active path.
     *
     * @return string
     */
    public function getPath() : string;

    /**
     * Get Domain name.
     *
     * @return string
     */
    public function getDomain() : string;

    /**
     * Get Secure switch.
     *
     * @return bool
     */
    public function getSecure() : bool;

    /**
     * Return Http Only.
     *
     * @return bool
     */
    public function getHttpOnly() : bool;
}
