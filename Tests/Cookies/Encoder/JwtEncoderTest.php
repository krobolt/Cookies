<?php

namespace Tests\Cookies\Encoder;

use Ds\Cookies\Encoder\JwtEncoder;
use Ds\Jwt\JwtInterface;

class JwtEncoderTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var NullEncoder
     */
    public $encoder;

    /**
     * @var JwtEncoder
     */
    public $jwt;


    /**
     * Set up.
     */
    public function setUp()
    {

        $this->jwt = $this->getMockBuilder(JwtInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->encoder = new JwtEncoder($this->jwt, 'my-secret');
    }

    public function testEncode(){

        $expected  = 'my-enc-jwt-token';

        $dateTime = $this->getMockBuilder('\DateTime')->getMock();

        $options = [
            'expire' => $dateTime,
            'domain' => 'example.com'
        ];

        $expectedTime = strtotime('Jan 13 2017');

        $this->jwt->expects($this->once())
            ->method('createFromArray')
            ->willReturn($this->jwt);

        $this->jwt->expects($this->once())
            ->method('withNotBefore')
            ->willReturn($this->jwt);

        $this->jwt->expects($this->once())
            ->method('withExpires')
            ->willReturn($this->jwt);

        $dateTime->expects($this->once())
            ->method('getTimestamp')
            ->willReturn($expectedTime);

        $this->jwt->expects($this->once())
            ->method('withAudience')
            ->willReturn($this->jwt);

        $this->jwt->expects($this->once())
            ->method('getToken')
            ->willReturn($expected);

        $this->encoder->encode(['data'],$options);

    }

}
