<?php

namespace Tests\Cookies\Encoder;

use Ds\Cookies\Encoder\NullEncoder;

class NullEncoderTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var NullEncoder
     */
    public $encoder;

    /**
     * Set up.
     */
    public function setUp()
    {
        $this->encoder = new NullEncoder();
    }

    public function testEncode(){
        $value = 'my-value';
        $expected = $value;
        $actual = $this->encoder->encode($value);
        $this->assertEquals($expected, $actual);
    }

    public function testDecode(){
        $value = 'my-value';
        $expected = $value;
        $actual = $this->encoder->decode($value);
        $this->assertEquals($expected, $actual);
    }
}
