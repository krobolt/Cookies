<?php

namespace Tests\Cookies;

use Ds\Cookies\CookieAdaptorInterface;
use Ds\Cookies\Cookies;
use Ds\Cookies\Encoder\JwtEncoder;

/**
 * Class CookiesTest
 * @package Tests\Cookies
 */
class CookiesTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Cookies $cookies
     */
    public $cookies;

    /**
     * @var CookieAdaptorInterface $adaptor
     */
    public $adaptor;

    /**
     * @var JwtEncoder $encoder
     */
    public $encoder;

    /**
     * Set up.
     */
    public function setUp()
    {

        $this->adaptor = $this->getMockBuilder(CookieAdaptorInterface::class)
            ->getMock();

        $this->encoder = $this->getMockBuilder(JwtEncoder::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->cookies = new Cookies(
            $this->adaptor,
            $this->encoder
        );
    }

    /**
     *
     */
    public function testSetCookie(){

        $name = 'cookie';
        $value = 'value';
        $expire = new \DateTime('now + 1 day');
        $path = '/';
        $domain = 'domain';
        $secure = false;
        $httpOnly = false;

        $this->encoder->expects($this->once())
            ->method('encode')
            ->with(
                $this->equalTo($value)
            )
            ->willReturn($value);

        $this->adaptor->expects($this->once())
            ->method('setCookie')
            ->with(
                $this->equalTo($name),
                $this->equalTo($value),
                $this->equalTo($expire),
                $this->equalTo($path),
                $this->equalTo($domain),
                $this->equalTo($secure),
                $this->equalTo($httpOnly)
            );

        $this->cookies->setCookie($name,$value,$expire,$path,$domain,$secure,$httpOnly);
    }

    /**
     *
     */
    public function testHasCookie(){

        $name = 'cookie';
        $expected = true;

        $this->adaptor->expects($this->once())
            ->method('hasCookie')
            ->with(
                $this->equalTo($name)
            )
            ->willReturn($expected);

        $actual = $this->cookies->hasCookie($name);
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testHasNoCookie(){

        $name = 'cookie';
        $expected = false;

        $this->adaptor->expects($this->once())
            ->method('hasCookie')
            ->with(
                $this->equalTo($name)
            )
            ->willReturn($expected);

        $actual = $this->cookies->hasCookie($name);
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testGetCookies(){

        $expected = [];

        $this->adaptor->expects($this->once())
            ->method('getCookies')
            ->willReturn($expected);

        $actual = $this->cookies->getCookies();
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test Get Cookies
     */
    public function testGetCookie(){

        $cookieName = 'cookieName';
        $cookieValue = 'my-cookie-value';
        $expected = 'decoded-cookie';

        $this->adaptor->expects($this->once())
            ->method('getCookie')
            ->with(
                $this->equalTo($cookieName)
            )
            ->willReturn($cookieValue);

        $this->encoder->expects($this->once())
            ->method('decode')
            ->with(
                $this->equalTo($cookieValue)
            )
            ->willReturn($expected);

        $actual = $this->cookies->getCookie($cookieName);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test Delete Cookies
     */
    public function testDeleteCookie(){
        $cookieName = 'cookieName';
        $value = 'value';
        $expire = new \DateTime('now + 1 day');
        $path = '/';
        $domain = 'domain';
        $secure = false;
        $httpOnly = false;

        $this->adaptor->expects($this->once())
            ->method('deleteCookie')
            ->with(
                $this->equalTo($cookieName),
                $this->equalTo($path),
                $this->equalTo($domain),
                $this->equalTo($secure),
                $this->equalTo($httpOnly)
            );

        $this->cookies->deleteCookie($cookieName,$path,$domain,$secure,$httpOnly);
    }

    /**
     * Test refresh cookie without a cookie set.
     */
    public function testRefreshCookieNoCookie(){
        $name = 'cookie';
        $expected = false;
        $this->hasCookieExpectations($name, $expected);
        $actual = $this->cookies->refreshCookie($name, new \DateTime('now + 1 day'));
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test Refresh cookie with cookie present.
     */
    public function testRefreshCookieWithCookie(){

        $name = 'cookie';
        $value = 'value';
        $expire = new \DateTime('now + 1 day');
        $path = '/';
        $domain = 'domain';
        $secure = false;
        $httpOnly = false;

        $expected = true;

        $this->hasCookieExpectations($name, $expected);
        $this->getCookieExpectations($name,$value);
        $this->setCookieExpectations($name,$value,$expire,$path,$domain,$secure,$httpOnly);

        $actual = $this->cookies->refreshCookie($name,$expire,$path,$domain,$secure,$httpOnly);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Cookie:hasCookie expectations
     *
     * @param string $name
     * @param bool $expected
     */
    private function hasCookieExpectations(string $name, bool $expected){
        $this->adaptor->expects($this->once())
            ->method('hasCookie')
            ->with(
                $this->equalTo($name)
            )
            ->willReturn($expected);
    }

    /**
     * Cookie:getCookie expectations
     *
     * @param string $name
     * @param $value
     */
    private function getCookieExpectations(string $name, $value){

        $this->adaptor->expects($this->once())
            ->method('getCookie')
            ->with(
                $this->equalTo($name)
            )
            ->willReturn($value);

        $this->encoder->expects($this->once())
            ->method('decode')
            ->with(
                $this->equalTo($value)
            )
            ->willReturn($value);
    }

    /**
     * Cookie:setCookie expectations
     *
     * @param $name
     * @param $value
     * @param $expire
     * @param $path
     * @param $domain
     * @param $secure
     * @param $httpOnly
     */
    private function setCookieExpectations($name,$value,$expire,$path,$domain,$secure,$httpOnly){
        $this->encoder->expects($this->once())
            ->method('encode')
            ->with(
                $this->equalTo($value)
            )
            ->willReturn($value);

        $this->adaptor->expects($this->once())
            ->method('setCookie')
            ->with(
                $this->equalTo($name),
                $this->equalTo($value),
                $this->equalTo($expire),
                $this->equalTo($path),
                $this->equalTo($domain),
                $this->equalTo($secure),
                $this->equalTo($httpOnly)
            );
    }
}
