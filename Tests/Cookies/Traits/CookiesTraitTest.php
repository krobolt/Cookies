<?php

namespace Tests\Cookies\Traits;

use Ds\Cookies\Adaptor\CookieAdaptor;
use Ds\Cookies\Cookies;
use Ds\Cookies\Encoder\JwtEncoder;
use Ds\Cookies\CookieAdaptorInterface;

/**
 * Class CookiesTraitTest
 * @package Tests\Cookies\Traits
 */
class CookiesTraitTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Cookies
     */
    public $cookies;

    /**
     * @var CookieAdaptorInterface $adaptor
     */
    public $adaptor;

    /**
     * @var JwtEncoder $encoder
     */
    public $encoder;

    /**
     * @var \DateTime
     */
    public $expire;

    /**
     * @var string
     */
    public $path = '/path';

    /**
     * @var string
     */
    public $domain = 'www.domain.com';

    /**
     * @var bool
     */
    public $secure = false;

    /**
     * @var bool
     */
    public $httpOnly = false;

    /**
     * Set up.
     */
    public function setUp()
    {

        $this->adaptor = $this->getMockBuilder(CookieAdaptorInterface::class)
            ->setMethods(['getDefaults','setCookie','hasCookie','getCookies','withCookies','getCookie','deleteCookie'])
            ->getMock();

        $this->encoder = $this->getMockBuilder(JwtEncoder::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->cookies = new Cookies(
            $this->adaptor,
            $this->encoder
        );

        $this->expire = new \DateTime('10 Jan 2017');

        $this->cookies =
            $this->cookies->withDefaults(
                $this->expire,
                $this->path,
                $this->domain,
                $this->secure,
                $this->httpOnly
            );

    }

    /**
     *
     */
    public function testWithExpire(){
        $expected = new \DateTime('20 Jan 2017');

        $cookie = $this->cookies->withExpire($expected);
        $actual = $cookie->getExpire();

        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testWithDomain(){
        $expected = 'http://example.com';

        $cookie = $this->cookies->withDomain($expected);
        $actual = $cookie->getDomain();

        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testWithPath(){
        $expected = '/newpath';

        $cookie = $this->cookies->withPath($expected);
        $actual = $cookie->getPath();

        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testWithSecure(){
        $expected = true;

        $cookie = $this->cookies->withSecure($expected);
        $actual = $cookie->getSecure();

        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testWithWithHttpOnly(){
        $expected = true;

        $cookie = $this->cookies->withDomain($expected);
        $actual = $cookie->getDomain();

        $this->assertEquals($expected, $actual);
    }
}
