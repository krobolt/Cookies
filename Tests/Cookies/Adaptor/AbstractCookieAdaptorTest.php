<?php

namespace Tests\Cookies\Adaptor;

use Ds\Cookies\Adaptor\AbstractCookieAdaptor;

class CookieAdaptor extends AbstractCookieAdaptor{
    public function __construct($cookies)
    {
        $this->cookies = $cookies;
    }
}

class AbstractCookieAdaptorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var CookieAdaptor $adaptor
     */
    public $adaptor;

    public $cookies;

    /**
     * Set up.
     */
    public function setUp()
    {
        $this->cookies = [
            'cookie' => 'value',
            'anotherCookie' => 'someOtherValue'
        ];

        $this->adaptor = new CookieAdaptor($this->cookies);
    }


    public function testWithCookies(){

        $newCookies = [
            'c' => 1,
            'c2' => 2,
            'c3' => 'safecookievalue'
        ];

        $expected = array_merge($newCookies, $this->cookies);

        $this->adaptor = $this->adaptor->withCookies($newCookies);
        $this->assertEquals($this->adaptor->getCookies(), $expected);
    }

    public function testGetCookies(){
        $this->assertEquals($this->cookies, $this->adaptor->getCookies());
    }

    public function testHasCookie(){
        $cookies = ['cookie' => 'value'];
        $this->adaptor = $this->adaptor->withCookies($cookies);
        $this->assertEquals(true, $this->adaptor->hasCookie('cookie'));
    }

    public function testHasNoCookie(){
        $cookies = ['cookie' => 'value'];
        $this->adaptor = $this->adaptor->withCookies($cookies);
        $this->assertEquals(false, $this->adaptor->hasCookie('no-cookie'));
    }

    public function testGetCookie(){
        $cookies = ['cookie' => 'value'];
        $expected = $cookies['cookie'];
        $this->adaptor = $this->adaptor->withCookies($cookies);
        $this->assertEquals($expected, $this->adaptor->getCookie('cookie'));
    }

}
