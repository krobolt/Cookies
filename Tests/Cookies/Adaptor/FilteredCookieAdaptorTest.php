<?php

namespace Tests\Cookies\Adaptor;

use Ds\Cookies\Adaptor\FilteredCookieAdaptor;

class FilteredCookieAdaptorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var FilteredCookieAdaptor $adaptor
     */
    public $adaptor;

    /**
     * Set up.
     */
    public function setUp()
    {
        $this->adaptor = new FilteredCookieAdaptor();
    }

    public function testHasCookieUnsafeFiltered(){
        $cookies = ['cookie' => '<?php var=phpinfo()'];
        $this->adaptor = $this->adaptor->withCookies($cookies);
        $this->assertEquals(false, $this->adaptor->hasCookie('cookie'));
    }

    public function testWithCookiesFilter(){
        $newCookies = [
            'c' => 1,
            'c2' => 2,
            'c3' => '<?php var=phpinfo()'
        ];

        $expected = $newCookies;
        $expected['c3'] = null;

        $this->adaptor = $this->adaptor->withCookies($newCookies);
        $this->assertEquals($this->adaptor->getCookies(), $expected);
    }

    public function testGetCookie(){
        $cookies = ['cookie' => 'value'];
        $expected = filter_var($cookies['cookie']);
        $this->adaptor = $this->adaptor->withCookies($cookies);
        $this->assertEquals($expected, $this->adaptor->getCookie('cookie'));
    }
}
